
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 04/08/2014 23:13:53
-- Generated from EDMX file: G:\Dropbox\Visual Studio 2012\Projects\MMTSTrainData\MMTSTrainData\TrainData.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TrainData];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[RunningTrain]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RunningTrain];
GO
IF OBJECT_ID(N'[dbo].[Station]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Station];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'RunningTrain'
CREATE TABLE [dbo].[RunningTrain] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TrainNumber] nvarchar(10)  NOT NULL,
    [StationName] nvarchar(30)  NOT NULL,
    [ArrivalTime] nvarchar(10)  NOT NULL
);
GO

-- Creating table 'Station'
CREATE TABLE [dbo].[Station] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [StationName] nvarchar(30)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'RunningTrain'
ALTER TABLE [dbo].[RunningTrain]
ADD CONSTRAINT [PK_RunningTrain]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Station'
ALTER TABLE [dbo].[Station]
ADD CONSTRAINT [PK_Station]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------