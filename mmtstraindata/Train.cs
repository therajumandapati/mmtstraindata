﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMTSTrainData
{
    class Train
    {
        public string trainname { get; set; }
        public string stationname { get; set; }
        public string arrivaltime { get; set; }
        public bool sunday { get; set; }
    }
}
