﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MMTSTrainData
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.GetTrainData();
            
        }

        public async void GetTrainData()
        {
            Console.WriteLine("Grabbing data - 1.......");
            List<Train> mainList = new List<Train>();
            for (int i = 47100; i <= 47220; i++)
            {
                Console.WriteLine("Train no: " + i);
                //hit the api and save it to class
                HttpClient client = new HttpClient();
                string json;
                try
                {
                    json = GetString("http://www.mmtstraintimings.in/MMTSTimingService.svc/trains/" + i);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                    break;
                }
                
                List<Train> trainList = JsonConvert.DeserializeObject<List<Train>>(json);
                try
                {
                    foreach (var train in trainList)
                    {
                        //write it to a list
                        mainList.Add(train);
                        using (var context = new TrainDBEntities())
                        {
                            RunningTrain rt = new RunningTrain { ArrivalTime = train.arrivaltime, StationName = train.stationname, TrainNumber = train.trainname };
                            if (!FindIfRunningTrainExists(rt))
                            {
                                context.RunningTrain.Add(rt);
                            }
                            if (!FindIfStationExists(train.stationname))
                            {
                                Station stat = new Station { StationName = train.stationname };
                                context.Station.Add(stat);
                            }
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception e) 
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                }
                
            }

            Console.WriteLine("Grabbing data - 2.......");
            List<int> subtrains = new List<int>{57131, 57307, 57308, 57519, 67275, 67276, 67277, 67278, 77601, 77638, 77639, 77640, 77641, 77642, 77643, 77645, 77647, 77648, 77649, 77679, 77680, 77681, 77682};
            for (int i = 0; i < subtrains.Count; i++)
            {
                Console.WriteLine("Train no: " + subtrains.ToArray()[i]);
                //hit the api and save it to class
                HttpClient client = new HttpClient();
                string json;
                try
                {
                    json = GetString("http://www.mmtstraintimings.in/MMTSTimingService.svc/trains/" + subtrains[i]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                    break;
                }

                List<Train> trainList = JsonConvert.DeserializeObject<List<Train>>(json);
                try
                {
                    foreach (var train in trainList)
                    {
                        //write it to a list
                        mainList.Add(train);
                        using (var context = new TrainDBEntities())
                        {
                            RunningTrain rt = new RunningTrain { ArrivalTime = train.arrivaltime, StationName = train.stationname, TrainNumber = train.trainname };
                            if (!FindIfRunningTrainExists(rt))
                            {
                                context.RunningTrain.Add(rt);
                            }
                            if (!FindIfStationExists(train.stationname))
                            {
                                Station stat = new Station { StationName = train.stationname };
                                context.Station.Add(stat);
                            }
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                }

            }

            Console.WriteLine("Grabbing data - 3.......");
            for (int i = 77602; i <= 77635; i++)
            {
                Console.WriteLine("Train no: " + i);
                //hit the api and save it to class
                HttpClient client = new HttpClient();
                string json;
                try
                {
                    json = GetString("http://www.mmtstraintimings.in/MMTSTimingService.svc/trains/" + i);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                    break;
                }

                List<Train> trainList = JsonConvert.DeserializeObject<List<Train>>(json);
                try
                {
                    foreach (var train in trainList)
                    {
                        //write it to a list
                        mainList.Add(train);
                        using (var context = new TrainDBEntities())
                        {
                            RunningTrain rt = new RunningTrain { ArrivalTime = train.arrivaltime, StationName = train.stationname, TrainNumber = train.trainname };
                            if (!FindIfRunningTrainExists(rt))
                            {
                                context.RunningTrain.Add(rt);
                            }
                            if (!FindIfStationExists(train.stationname))
                            {
                                Station stat = new Station { StationName = train.stationname };
                                context.Station.Add(stat);
                            }
                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                }

            }
            Console.WriteLine("Done grabbing data!");
            Console.WriteLine("Enter to exit.....");
            Console.ReadLine();
        }

        public bool FindIfStationExists(string stationName) 
        {
            try
            {
                using (var context = new TrainDBEntities())
                {
                    var existingStat = (from s in context.Station
                                        where s.StationName == stationName
                                        select s).ToList();
                    if (existingStat.Count != 0)
                        return true;
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool FindIfRunningTrainExists(RunningTrain rt)
        {
            try
            {
                using (var context = new TrainDBEntities())
                {
                    var existingStat = (from r in context.RunningTrain
                                        where r.TrainNumber == rt.TrainNumber
                                        where r.ArrivalTime == rt.ArrivalTime
                                        where r.StationName == rt.StationName
                                        select r).ToList();
                    if (existingStat.Count != 0)
                        return true;
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static string GetString(string uri)
        {

            var httpClient = new HttpClient();

            return httpClient.GetStringAsync(uri).Result;
        }
    }
}
